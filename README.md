# FeniX Engine RPG Maker MV plugins
This branch will contain all FeniX Engine plugins for RPG Maker MV game engine.

![Standard - JavaScript Style Guide](https://cdn.rawgit.com/feross/standard/master/badge.svg)

## Installation

Currently unavailable for use in RPG Maker MV projects unless transpiled with BabelJS.
If you know how to use basic `git` and `npm` commands then proceed to installation for developers.

### Prerequisites

All commands are run in the Command Prompt or Powershell
#### Windows

- Install [NodeJS](https://nodejs.org/en/)
- Install [Git](https://git-scm.com/)

#### Ubuntu

- Install [NodeJS](https://nodejs.org/en/)

```sudo apt-get install nodejs```

- Install [npm](https://www.npmjs.com/)

```sudo apt-get install npm```

- Install [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

```sudo apt-get install git```

### Installation For Contributers/Developers
It's really easy to get started with FeniX Engine development
#### Forking And Cloning

- [Fork](https://gitlab.com/FeniXEngineMV/plugins/forks/new) the repo to your own account.

- Clone the repo to a directory on your machine

 `git clone git@gitlab.com:UserName/plugins.git`

For a basic how-to on cloning read this [Basic git commands](https://docs.gitlab.com/ee/gitlab-basics/command-line-commands.html)


#### Install Required Packages

Once cloned, change to the root directory of the project and open the command line/terminal and install the packages required for FeniX plugin development

 `npm install`

Next start the server

`npm run server`

*Running the server will build all files in `src/` to `game/js/plugins`, starts a server using Browsersync and opens your browser to play the game. All changes made in the `./src/` folder will refresh the browser so you can see the latest changes instantly.*

# Credits

All current plugins were previously developed by LTN Games over a year ago, after some deep thinking he decided that he would open source the entire project and rebrand it to FeniX Engine.

 While this is open source and credit is not required, it would be appreciated it you linked to FeniX Engine's website or GitLab repo to show support.

https://fenixenginemv.gitlab.io/

All plugins developed for FeniX engine must retain its license information in the header of all plugins and cannot be removed.

# Contributing
Please read over the [Contribution Guide](https://gitlab.com/LTNGames/plugins/blob/dev/CONTRIBUTING.md)

# License
[The MIT License](https://gitlab.com/LTNGames/plugins/blob/dev/LICENSE.md)