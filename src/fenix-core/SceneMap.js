
/**
 *
 * @file SceneMAp
 *
 * @author       FeniXEngine Contributers
 * @copyright    2018 FeniX Engine
 * @license      {@link https://gitlab.com/FeniXEngineMV/plugins/blob/develop/LICENSE|MIT License}
 */
import { _Plugin, Signals } from './Core'

/**
 * Scene_Map  >>
 *
 * Add CoreSignal for when the game map is loaded. Signals, events created and map ready
*/
_Plugin.addAlias('Scene_Map.onMapLoaded', Scene_Map.prototype.onMapLoaded)

Scene_Map.prototype.onMapLoaded = function () {
  _Plugin.callAlias('Scene_Map.onMapLoaded', this)
  Signals.emit('gamemap-loaded', this)
}
