/**
 * The plugins Core file, which contains registration and export of important
 * members.
 *
 * @file Core
 *
 * @author       FeniXEngine Contributers
 * @copyright    2018 FeniX Engine
 * @license      {@link https://gitlab.com/FeniXEngineMV/plugins/blob/develop/LICENSE|MIT License}
 */
import { PluginStore } from './PluginStore'
import { Signals as _Signals } from 'fenix-tools'

/**
 * The version of FeniXCore
 * @memberof FeniX
*/
export const VERSION = '2.0.0'

PluginStore.register({
  name: 'Core',
  version: VERSION,
  author: 'FeniXEngine'
})

/**
 * For use with core modules, for easy access to the core plugin client API.
 * @private
 */
export const _Plugin = PluginStore.require('Core')

/**
 * A signal dispatcher, used for emitting signals when important game events
 * occur.
 * @memberof FeniX
 */
export const Signals = _Signals()
