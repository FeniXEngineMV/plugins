/**
 *
 * @file SceneManager
 *
 * @author       FeniXEngine Contributers
 * @copyright    2018 FeniX Engine
 * @license      {@link https://gitlab.com/FeniXEngineMV/plugins/blob/develop/LICENSE|MIT License}
 */
/**
 * Checks the current scenes constructor with the one given
 *
 * @param {*} sceneClass - The scene class to compare to
 */
SceneManager.isScene = function (sceneClass) {
  return SceneManager._scene.constructor === sceneClass
}

/**
 * Checks if the current scene is an instance of the scene in the argument.
 *
 * @param {*} sceneClass - The scene class to check
 */
SceneManager.isSceneOf = function (sceneClass) {
  return SceneManager._scene instanceof sceneClass
}
