/**
 * This module extends the functionality of MV's PluginManager class
 *
 * @module PluginManager
 * @see module:PluginManager
 *
 * @author       FeniXEngine Contributers
 * @copyright    2018 FeniX Engine
 * @license      {@link https://gitlab.com/FeniXEngineMV/plugins/blob/dev/LICENSE|MIT License}
 */

/* globals $plugins */

import { convertParameters } from 'fenix-tools'

/**
  * Retrieves the plugins parameters, with their types converted.
  *
  * @function getParameters
  *
  * @param {string} plugin - The name of the plugin.
  *
  * @returns Returns an object with all converted plugin parameters.
  *
  * @example
  * /* @plugindesc My plugin description <PluginName> *\
  *
  * const pluginParams = PluginManager.getParameters('PluginName')
  *
  * console.log(pluginParams.myParam) // => Returns parameter value
  */
PluginManager.getParameters = function (plugin) {
  const rawParameters = $plugins.filter((p) => p.description.contains(`<${plugin}>`))[0].parameters
  return convertParameters(rawParameters)
}

/**
 * Will check if a plugin in the global $plugins object contains the plugin
 * tag in the description.
 *
 * @function isPlugin
 *
 * @param {string} plugin - The id of the plugin.
 *
 * @returns Returns true if the plugin's tag is found.
 * @example
 * /*: @plugindesc My plugin description <PluginName> *\
 *
 * console.log(PluginManager.isPlugin('PluginName)) // => Returns true
 */
PluginManager.isPlugin = function (plugin) {
  const pluginRegistered = $plugins.filter((p) => p.description.contains('<' + plugin + '>'))
  if (pluginRegistered.length <= -1) {
    return false
  }
  return true
}

/**
 * An array of all commands registered with the PluginManager
 * @internal
 */
PluginManager._commands = []

/**
 * Registers a plugin command with MV
 *
 * @function addCommand
 *
 * @param {string} plugin - The id of the plugin.
 *
 * @returns Returns true if the plugin's tag is found.
 */
PluginManager.addCommand = function (command, actions) {
  if (this.isCommand(command)) {
    throw new Error(`Command: ${command} is already used by another plugin`)
  }
  this._commands[command] = actions
}

/**
 * Checks if the command is registered with MV
 *
 * @function isCommand
 *
 * @param {string} command - The id of the plugin.
 *
 * @returns Returns true if the plugin's tag is found.
 */
PluginManager.isCommand = function (command) {
  return typeof this._commands[command] !== 'undefined'
}

/**
* Checks if the command is registered with MV
*
* @function getCommand

* @param {string} command - The id of the plugin.
*
* @returns Returns true if the command is found.
*/
PluginManager.getCommand = function (command) {
  if (this.isCommand(command)) {
    return this._commands[command]
  }
}
