/**
 *
 * @file GameInterpreter
 *
 * @author       FeniXEngine Contributers
 * @copyright    2018 FeniX Engine
 * @license      {@link https://gitlab.com/FeniXEngineMV/plugins/blob/develop/LICENSE|MIT License}
 */
import {_Plugin} from './Core'
_Plugin.addAlias('Game_Interpreter.pluginCommand', Game_Interpreter.prototype.pluginCommand)

Game_Interpreter.prototype.pluginCommand = function (command, args) {
  const actions = PluginManager.getCommand(command)
  if (actions) {
    try {
      const action = actions[args[0]]
      if (typeof action === 'function') {
        action.apply(null, {
          args: args[3],
          mapId: this._mapId,
          eventId: this._eventId,
          list: this._list
        })
      } else {
        throw new Error(`Command's action must be a function`)
      }
    } catch (error) {
      throw new Error(error)
    }
  } else {
    _Plugin.callAlias('Game_Interpreter.pluginCommand')
  }
}
