/**
 * Register and store plugins. Each plugin registered is assigned a plugin api
 * which lets you interact with its data.
 *
 * @module PluginStore
 *
 * @see module:PluginStore
 * @example
 * FeniX.PluginStore.register({
 *   name: 'MyPlugin',
 *   author: 'My Name',
 *   version: '1.0.0',
 *   required: []
 * })
 * .then(pluginClient => {
 *     console.log(pluginClient); // Prints the plugin api client.
 *   })
 * .catch(err => {
 *  console.log(err) // Prints error message for not registering
 * })
 *
 * @author       LTNGames <ltngamesdevelopment@gmail.com>
 * @copyright    2018 FeniX Engine
 * @license      {@link https://gitlab.com/FeniXEngineMV/plugins/blob/dev/LICENSE|MIT License}
 */

/**
 * The main plugins object that stores all registered plugins and their data.
 *
 */
const _plugins = {}

/**
 * Helper function to check if value is defined
 *
 * @param {any} value - The value to check
 *
 * @returns Returns true if value is defined
 *
 */
const _isDefined = (value) => typeof value !== 'undefined'

/**
 * Will check if a plugin is registered with FeniX plugins by checking if it's
 * defined in the _plugins object
 *
 * @param {String} plugin - The name of the plugin.
 *
 * @returns Return true if plugin is registered in FNX.Plugins.
 *
 *  module:PluginStore._isRegistered
 */
const _isRegistered = (name) => _isDefined(_plugins[name])

/**
 * The base plugin API client
 *
 * @param {any} config - The configuration for plugin
 *
 * @returns A new api object
 *
 */
const _PluginApi = function (config) {
  const _name = config.name || `Plugin-${Math.random(1000) * 22}`
  const _aliases = {}
  const _saveData = {}
  const _version = config.version || 'Unknown'
  const _author = config.author || 'Unknown'
  const _required = config.required || []

  return {
    get name () {
      return _name
    },

    get version () {
      return _version
    },

    get required () {
      return _required
    },

    get author () {
      return _author
    },

    get parameters () {
      return PluginManager.getParameters(`<FNX_${config.name}>`)
    },

    addToSaveData (key, data, overwrite = false) {
      if (_isDefined(_saveData[key]) && overwrite) {
        _saveData[key] = data
      } else if (_isDefined(_saveData[key]) === false) {
        _saveData[key] = data
      } else if (_isDefined(_saveData[key])) {
        console.error('Save data already exists. Use overwrite to force change values')
      }
    },

    getSaveData (key = '!all') {
      if (key === '!all') {
        return _saveData
      }
      if (_isDefined(_saveData[key])) {
        return _saveData[key]
      } else {
        console.error('Unable to find save data for plugin')
      }
    },

    addAlias (aliasName, originalFunc) {
      if (_isDefined(_aliases[aliasName])) {
        console.error(`Alias => ${aliasName} <= already exists for plugin => ${_name}`)
      } else {
        _aliases[aliasName] = originalFunc
      }
    },

    callAlias (aliasName, context, ...args) {
      if (_isDefined(_aliases[aliasName])) {
        try {
          return _aliases[aliasName].call(context, ...args)
        } catch (error) {
          throw new Error(`Unabel to call => ${aliasName} <= for plugin => ${_name} <=`)
        }
      } else {
        console.error(`Unable to find alias => ${aliasName} <= for plugin => ${_name} <=`)
      }
    }
  }
}

/**
 * Registers the plugin with PluginStore
 *
 * @param  {object} [config={}] - A plugin config object.
 *
 * @returns {promise} A promise which returns a newly created plugin API client.
 *
 * module:PluginStore.register
 */
function register (config = {}) {
  return new Promise((resolve, reject) => {
    if (!config.name) {
      reject(new Error(`Missing name for plugin registration`))
    }
    if (_isRegistered(config.name)) {
      reject(new Error(`Plugin already registered with PluginManager`))
    }
    _plugins[config.name] = _PluginApi(config)

    if (config.required) {
      config.required.forEach((plugin) => {
        if (!_isRegistered(plugin)) {
          reject(new Error(`You need to have the required plugin ${plugin}
              for ${config.name} to work correctly.`))
        }
      })
    }
    resolve(_plugins[config.name])
  })
}

/**
 * Retrieves plugins api for access to the plugins internal data.
 *
 * @param {object} name - Name of the plugin you want to require.
 *
 * @returns {_pluginApi} Return the plugins api object.
 * module:PluginStore.require
 * @example
 *
 * const _Plugin = PluginStore.require('MyPlugin') // => The plugin's client API
 *
 */
function require (name = 'all') {
  if (name === 'all') {
    return _plugins
  } else if (_isRegistered(name)) {
    return _plugins[name]
  }
}

export const PluginStore = { register, require }
