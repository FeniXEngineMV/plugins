/**
 *
 * @file DataManager
 *
 * @author       FeniXEngine Contributers
 * @copyright    2018 FeniX Engine
 * @license      {@link https://gitlab.com/FeniXEngineMV/plugins/blob/develop/LICENSE|MIT License}
 */
import { _Plugin, Signals } from './Core'
import { PluginStore } from './PluginStore'
import { loadEventComments } from 'fenix-tools'

_Plugin.addAlias('DataManager.onLoad', DataManager.onLoad)
_Plugin.addAlias('DataManager.setupNewGame', DataManager.setupNewGame)
_Plugin.addAlias('DataManager.makeSaveContents', DataManager.makeSaveContents)
_Plugin.addAlias('DataManager.extractSaveContents', DataManager.extractSaveContents)

/**
 * Assign save data to original data reference in plugin object.
 *
 * @function _assignSaveData
 *
 * @param {Object} contents - The save data object from MV's save data file.
 *
 * @memberOf PluginStore
 */
const _assignSaveData = function (contents) {
  const _allPlugins = PluginStore.require()
  const _dataToAssign = Object.values(_allPlugins).filter(
    plugin => typeof contents[plugin.name] !== 'undefined')

  try {
    _dataToAssign.forEach(plugin => {
      const saveFileContents = contents[plugin.name]
      const saveDataKeys = Object.keys(saveFileContents)

      Object.values(saveFileContents).forEach(
        (data, index) => {
          plugin.addToSaveData(saveDataKeys[index], data, true)
        })
    })
    Signals.emit('game-load-success')
  } catch (error) {
    Signals.emit('game-load-error', error)
    throw new Error(`Error assigning save data to original plugin ${error}`)
  }
}

DataManager.makeSaveContents = function () {
  const contents = _Plugin.callAlias('DataManager.makeSaveContents', this)
  const plugins = PluginStore.require()
  Signals.emit('game-save', contents)

  for (const plugin of Object.values(plugins)) {
    const saveData = plugin.getSaveData()

    if (Object.keys(saveData).length > 0) {
      contents[plugin.name] = saveData
    }
  }
  return contents
}

DataManager.extractSaveContents = function (contents) {
  _Plugin.callAlias('DataManager.extractSaveContents', this, contents)
  Signals.emit('game-load', contents)
  _assignSaveData(contents)
}

DataManager.onLoad = function (object) {
  _Plugin.callAlias('DataManager.onLoad', this, object)
  if (object === $dataMap) {
    Signals.emit('comments-loaded', loadEventComments(), $dataMap)
    Signals.emit('map-loaded', $dataMap)
  }
}

DataManager.setupNewGame = function () {
  _Plugin.callAlias('DataManager.setupNewGame', this)
  Signals.emit('new-game')
}

DataManager.getAllComments = function () {
  return loadEventComments()
}
